'use strict'


let personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
    start: function() {
        let i = 0;
        let numberOfFilms = 0;

        while(i < 1) {
            numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "");
            if(numberOfFilms) {
                if(!isNaN(numberOfFilms) && numberOfFilms != '') {
                    i++;
                }
            }
        } 

        this.count = numberOfFilms;
    },
    rememberMyFilms: function() {
        let i = 0;
        while(i < 2) {
            let lastSeenFilm = prompt("Один из последних просмотренных фильмов?", "")
            if(lastSeenFilm) {
                if(lastSeenFilm.length <= 50 && lastSeenFilm != "" && isNaN(lastSeenFilm)) {
                    let filmRate = prompt("На сколько вы оценить его?", "");
                    this.movies[lastSeenFilm] = filmRate;
                    i++;
                }
            } 
        }
    }, 
    detectPersonalLevel: function() {

        if(this.count >= 0 && this.count < 10) {
            console.log("Просмотрено довольно мало фильмов");
        } else if(this.count >= 10 &&  this.count <= 30) {
            console.log("Вы классический зритель");
        } else if(this.count > 30) {
            console.log("Вы киноман");
        } else {
            console.log("Произошла ошибка");
        }
    },
    showMyDB: function() {
        if(!this.privat) {
            console.log(this)
        }
    },
    writeYourGenres: function() {
        let i = 0;
        while(i < 3) {
            let favoriteGenre = prompt(`Ваш любимый жанр под номером ${i + 1}`, "")
            if(favoriteGenre) {
                if(favoriteGenre.length <= 50 && favoriteGenre != "" && isNaN(favoriteGenre)) {
                    this.genres.push(favoriteGenre);
                    i++;
                }
            } 
        }

        this.genres.forEach((item, index) => {
            console.log(`Любимый жанр №${index + 1} - это (${item})`)
        });

    }, 
    toggleVisibleMyDB: function() {
        this.privat = !this.privat;
    }
}



personalMovieDB.start();
// personalMovieDB.rememberMyFilms();
// personalMovieDB.detectPersonalLevel();
// personalMovieDB.writeYourGenres();
personalMovieDB.showMyDB();
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();
personalMovieDB.writeYourGenres();