/* Задания на урок:

1) Удалить все рекламные блоки со страницы (правая часть сайта)

2) Изменить жанр фильма, поменять "комедия" на "драма"

3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
Реализовать только при помощи JS

4) Список фильмов на странице сформировать на основании данных из этого JS файла.
Отсортировать их по алфавиту 

5) Добавить нумерацию выведенных фильмов */

'use strict';

function printFilmList(filmList, block_to_print) {
    let movieHTMLList = '';

    filmList.sort().forEach((movie, index) => {
        movieHTMLList += `
        <li class="promo__interactive-item">
        ${index + 1}) ${movie}
            <div class="delete"></div>
        </li>`;
    }); 

    block_to_print.innerHTML = movieHTMLList;
}


const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};


let ads_banners = document.getElementsByClassName("promo__adv");
let filmTitle = document.getElementsByClassName("promo__genre")[0];
let background_image = document.getElementsByClassName("promo__bg")[0];
let filmList = document.getElementsByClassName("promo__interactive-list")[0];

background_image.style.cssText = "background-image: url('img/bg.jpg')";

filmTitle.innerText = "Драма";

printFilmList(movieDB.movies, filmList);

ads_banners[0].remove();





