'use strict'

let numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "");

console.log(numberOfFilms)

let personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
}


for(let i = 0; i < 2; i++) {

    let lastSeenFilm = prompt("Один из последних просмотренных фильмов?", "")
    if(lastSeenFilm) {
        if(lastSeenFilm.length <= 50 && lastSeenFilm != "") {
            let filmRate = prompt("На сколько вы оценить его?", "");
            personalMovieDB.movies[lastSeenFilm] = filmRate;
            continue;
        }
    } 

    i--;
}



// Примеры использования циклов while и do while представлены ниже (закомментированы)

// let i = 0;
// while(i < 2) {
//     let lastSeenFilm = prompt("Один из последних просмотренных фильмов?", "")
//     if(lastSeenFilm) {
//         if(lastSeenFilm.length <= 50 && lastSeenFilm != "") {
//             let filmRate = prompt("На сколько вы оценить его?", "");
//             movies[lastSeenFilm] = filmRate;
//             i++;
//         }
//     } 
// }

// let j = 0;

// do {
//     let lastSeenFilm = prompt("Один из последних просмотренных фильмов?", "")
//     if(lastSeenFilm) {
//         if(lastSeenFilm.length <= 50 && lastSeenFilm != "") {
//             let filmRate = prompt("На сколько вы оценить его?", "");
//             movies[lastSeenFilm] = filmRate;
//             j++;
//         }
//     } 
// } while(j < 2);


if(numberOfFilms > 0 && numberOfFilms < 10) {
    console.log("Просмотрено довольно мало фильмов");
} else if(numberOfFilms >= 10 && numberOfFilms <= 30) {
    console.log("Вы классический зритель");
} else if(numberOfFilms > 30) {
    console.log("Вы киноман");
} else {
    console.log("Произошла ошибка");
}
