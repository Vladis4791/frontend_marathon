'use strict'


function start() {

    let i = 0;
    let numberOfFilms = 0;

    while(i < 1) {
        numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "");
        if(numberOfFilms) {
            if(!isNaN(numberOfFilms) && numberOfFilms != '') {
                i++;
            }
        }
    } 

    console.log(numberOfFilms)

    let personalMovieDB = {
        count: Number(numberOfFilms),
        movies: {},
        actors: {},
        genres: [],
        privat: false
    }

    return personalMovieDB;

}


function rememberMyFilms(personData) {
    let i = 0;
    while(i < 2) {
        let lastSeenFilm = prompt("Один из последних просмотренных фильмов?", "")
        if(lastSeenFilm) {
            if(lastSeenFilm.length <= 50 && lastSeenFilm != "" && isNaN(lastSeenFilm)) {
                let filmRate = prompt("На сколько вы оценить его?", "");
                personData.movies[lastSeenFilm] = filmRate;
                i++;
            }
        } 
    }
}

function detectPersonalLevel(personalData) {
    let count = personalData.count

    if(count >= 0 && count < 10) {
        console.log("Просмотрено довольно мало фильмов");
    } else if(count >= 10 &&  count <= 30) {
        console.log("Вы классический зритель");
    } else if(count > 30) {
        console.log("Вы киноман");
    } else {
        console.log("Произошла ошибка");
    }
}


function showMyDB(personalData) {
    if(!personalData.privat) {
        console.log(personalData)
    }
}


function writeYourGenres(personalData) {
    let i = 0;
    while(i < 3) {
        let favoriteGenre = prompt(`Ваш любимый жанр под номером ${i + 1}`, "")
        if(favoriteGenre) {
            if(favoriteGenre.length <= 50 && favoriteGenre != "" && isNaN(favoriteGenre)) {
                personalData.genres.push(favoriteGenre);
                i++;
            }
        } 
    }
}


let data = start()
detectPersonalLevel(data)
rememberMyFilms(data);23
writeYourGenres(data);
showMyDB(data);