/* Задания на урок:

1) Реализовать функционал, что после заполнения формы и нажатия кнопки "Подтвердить" - 
новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;
P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.

2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки

3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)

4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: 
"Добавляем любимый фильм"

5) Фильмы должны быть отсортированы по алфавиту */

// Возьмите свой код из предыдущей практики

'use strict';


const movieDB = {
    movies: [
    ]
};


let ads_banners = document.getElementsByClassName("promo__adv");
let filmTitle = document.getElementsByClassName("promo__genre")[0];
let background_image = document.getElementsByClassName("promo__bg")[0];
let filmListBlock = document.getElementsByClassName("promo__interactive-list")[0];

// The last lesson

background_image.style.cssText = "background-image: url('img/bg.jpg')";

filmTitle.innerText = "Драма";

ads_banners[0].remove();


let addFilmButton = document.querySelector(".add button");
let addFilmInput = document.querySelector(".add .adding__input");
let checkbox = document.querySelector(".add input[type='checkbox']");

console.log(addFilmButton);


function printFilmList(filmList, blockToPrint) {

    blockToPrint.innerHTML = "";

    filmList.sort();

    filmList.forEach((movie, index) => {
        
        let shortTitle = movie;
        if(movie.length > 21) {
            shortTitle = trim_film_title(movie, 21);
        }
        
        let currentElement = createFilmTitleElement(`${index + 1}) ${shortTitle}`);
        currentElement.dataset.filmTitle = movie.toLowerCase();
        addEventListenerForDeleteButton(currentElement.querySelector(".delete"));

        blockToPrint.append(currentElement);
    });

}; 

function trim_film_title(film_title, len_to_trim) {
    let trimmed_film_title = film_title.substr(0, len_to_trim);
    return `${trimmed_film_title}...`;
}


function createFilmTitleElement(filmTitle) {
    let filmTitleElement = document.createElement('li');
    filmTitleElement.classList.add("promo__interactive-item");

    filmTitleElement.innerHTML = `${filmTitle}<div class="delete"></div>`;
    
    return filmTitleElement;
}


addFilmButton.addEventListener('click', (event) => {
    event.preventDefault();
    
    if(checkbox.checked) {
        console.log("Добавляем любимый фильм");
    };

    
    let val = addFilmInput.value;

    if(val.length > 0) {
        movieDB.movies.push(val);
        printFilmList(movieDB.movies, filmListBlock);
        addFilmInput.value = "";
    }
});


function delete_film_title(deleteButton) {
    // получить родителя
    let element_to_delete = deleteButton.parentElement;
    let filmTitle = element_to_delete.dataset.filmTitle;
    
    let myIndex = movieDB.movies.indexOf(filmTitle);

    console.log(myIndex);
    if (myIndex !== -1) {
        movieDB.movies.splice(myIndex, 1);
    }

    printFilmList(movieDB.movies, filmListBlock);
}



function addEventListenerForDeleteButton(deleteButton) {
    deleteButton.addEventListener('click', (e) => {
        delete_film_title(deleteButton);
    });
}
