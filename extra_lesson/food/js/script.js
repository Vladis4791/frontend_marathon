window.addEventListener('DOMContentLoaded', () => {
    
    // tabs 

    const tabs = document.querySelectorAll('.tabheader__item'),
    tabsContent = document.querySelectorAll(".tabcontent"),
    tabsParent = document.querySelector('.tabheader__items');

    function hideTabContent() {
        tabsContent.forEach((item) => {
            item.classList.add('hide');
            item.classList.remove('show');
        });

        tabs.forEach((item) => {
            item.classList.remove('tabheader__item_active');
        });
    }

    function showTabContent(i = 0) {
        tabsContent[i].classList.add("show");
        tabsContent[i].classList.remove("hide");

        tabs[i].classList.add("tabheader__item_active");

    }

    hideTabContent();
    showTabContent();

    tabsParent.addEventListener('click', (event) => {

        const target = event.target;

        tabs.forEach((item, index) => {
            if(target == item) {
                hideTabContent();
                showTabContent(index);
            }
        });
    });

    // timer

    const deadline = "2022-03-23";

    function getDatePrettyFormat(number) {
        if(number >= 0 && number < 10) {
            return `0${number}`;
        }

        return number;
    }

    function getRemainingTime(endtime) {
        const t = Date.parse(endtime) - Date.parse(new Date()),
        days = Math.floor(t / (1000 * 60 * 60 * 24)),
        hours = Math.floor(t / (1000 * 60 * 60) % 24),
        minutes = Math.floor(t / (1000 * 60) % 60),
        seconds = Math.floor(t / 1000 % 60);


        return {
            'total': t,
            days,
            hours,
            minutes,
            seconds
        }
    }

    function setClock(selector, endtime) {
        const timer = document.querySelector(selector),
        days = timer.querySelector('#days'),
        hours = timer.querySelector('#hours'),
        minutes = timer.querySelector('#minutes'),
        seconds = timer.querySelector('#seconds'),
        timeInterval = setInterval(updateClock, 1000);

        updateClock();

        function updateClock() {
            const t = getRemainingTime(endtime);

            if(t.total <= 0) {
                clearInterval(timeInterval);
            } else {

                days.innerHTML = getDatePrettyFormat(t.days);
                hours.innerHTML = getDatePrettyFormat(t.hours);
                minutes.innerHTML = getDatePrettyFormat(t.minutes);
                seconds.innerHTML = getDatePrettyFormat(t.seconds);

            }
            
        }

    }

    setClock('.timer', deadline);

    // Modal

    const openButtons = document.querySelectorAll("[data-open]"),
        closeButton = document.querySelector("[data-close]"),
        modal = document.querySelector(".modal");



    function closeModal() {
        modal.classList.add("hide");
        modal.classList.remove("show");
        document.body.style.overflow = '';
    }

    function openModal() {
        modal.classList.add("show");
        modal.classList.remove("hide");
        document.body.style.overflow = 'hidden';
        // clearInterval(modalTimerId);
    }

    function openModalByScroll() {
        if(window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight) {
            openModal();
            window.removeEventListener('scroll', openModalByScroll);
        }
    }


    openButtons.forEach((openButton) => {
        openButton.addEventListener('click', openModal);
    });

    closeButton.addEventListener('click', closeModal);

    modal.addEventListener('click', (e) => {
        if(e.target === modal) {
            closeModal();
        }
    });

    document.addEventListener('keydown', (e) => {
        if(e.code === "Escape" && modal.classList.contains("show")) {
            closeModal();
        }
    });

    // const modalTimerId = setTimeout(openModal, 5000);

    window.addEventListener('scroll', openModalByScroll);


    // Создание классов

    class MenuCard {
        constructor(src, alt, title, description, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.description = description;
            this.price = price;
            this.classes = classes;
            this.parent = document.querySelector(parentSelector);
            this.exchangeRate = 120;
            this.changeDollarsToRubbles();
        }

        changeDollarsToRubbles() {
            this.price = this.price * this.exchangeRate;
        }

        render() {
            const element = document.createElement('div');

            if(this.classes.indexOf("menu__item") == -1) {
                element.classList.add("menu__item")
            } 
            
            element.classList.add(...this.classes);
            

            element.innerHTML = `
                <img src=${this.src} alt=${this.alt}>
                <h3 class="menu__item-subtitle">${this.title}</h3>
                <div class="menu__item-descr">${this.description}</div>
                <div class="menu__item-divider"></div>
                <div class="menu__item-price">
                    <div class="menu__item-cost">Цена:</div>
                    <div class="menu__item-total"><span>${this.price}</span> руб/день</div>
                </div>

            `;

            this.parent.append(element);
        }
    }

    new MenuCard(
        "img/tabs/vegy.jpg",
        "vegy",
        'Меню "Фитнес"',
        'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
        10,
        '.menu .container',
        'big',
        'small'
    ).render();

    new MenuCard(
        "img/tabs/post.jpg",
        "post",
        'Меню "Постное"',
        'Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков.',
        13,
        '.menu .container',
        'menu__item'
    ).render();

    new MenuCard(
        "img/tabs/elite.jpg",
        "elite",
        'Меню “Премиум”',
        'В меню “Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!',
        12,
        '.menu .container',
        'menu__item'
    ).render();

});